(function($) {
  Drupal.behaviors.brilliant_gallery = {
    attach: function(context, settings) {

      // Bind a click event to visibility links.
      $('#bg-admin-edit').delegate('.bg-visibility-ajax', 'click', function() {
        var link = $(this);
        var action = link.html();

        // Indicate that visibility status is being updated.
        link.parents('td').siblings('.bg-visibility-status').html('<span style="color:red;">Saving...</span>');

        // Make an ajax request.
        $.get(link.attr('href'), {}, function(data) {

          if (data == true) {
            var new_action = (action == 'show' ? 'hide' : 'show');

            // Update the link text.
            link.html(new_action);

            // Update the link URL.
            link.attr('href', function(i, val) {
              return val.replace('action=' + action, 'action=' + new_action);
            });

            // Update the status text.
            link.parents('td').siblings('.bg-visibility-status').html(action == 'show' ? 'Visible' : 'Hidden');
          }
          else {
            link.parents('td').siblings('.bg-visibility-status').html('<span style="color:red;">Error</span>');
          }

        });

        // Prevent the link from being followed.
        return false;
      });

    }
  }
})(jQuery);