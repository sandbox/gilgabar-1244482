Brilliant Gallery
-----------------
Create lightbox enabled image galleries from directories of unmanaged files.

Installation
------------
- Create a directory within your public files directory that will contain your galleries.
  - Place directories filled with image files in that directory.  Each directory of images will constitute one gallery.
- Enable the Brilliant Gallery module at 'admin/modules'.
- Configure general settings and defaults at 'admin/config/media/brilliant_gallery'.
  - Set the path to your galleries directory.
- Visit 'admin/config/content/formats' and configure the text formats that you wish to use with Brilliant Gallery.
  - Check the 'Brilliant Gallery Tag' filter.
  - You may need to rearrange the filter processing order depending on which other filters you may have enabled.
- Visit 'admin/content/brilliant_gallery' to view and edit your galleries.
  - Select a gallery to view and edit the images within that gallery.

Usage
-----
To add a gallery to a node:
  - Create or edit a node.
  - Select a text format for the node body that you have configured to use Brilliant Gallery.
  - Enter a special Brilliant Gallery tag in the text area where you would like your gallery to appear.
  - Two styles of tags are supported:
    - the old style bg tags of the form: [bg|value1|value2].
      - Old style bg tags rely on the position of the parameters. Some parameters are no longer used, but to maintain backwards compatibility the parameters will still need to appear in the correct positions: 'location', 'columns', 'width', 'sort', 'limit', 'color', 'offset', 'caption'.
    - the new style bg tags of the form: [bg|parameter1=value1|parameter2=value2].
      - New style bg tags use named parameters. For booleans you may use anything that approximates a boolean value (true/false, yes/no, etc):
        - location: The location of the images. Usually a directory path.
          - Use '*' to display all images in all galleries.
          - Use '?' to display images from a random gallery.
        - recursive: A boolean to decide if images in subfolders should also be retrieved.
        - thumb: The image style to use to format the thumbnail size image.
        - full: The image style to use to format the full size image.
        - sort: A boolean to decide if the images should be sorted alphabetically by filename or shuffled randomly.
        - offset: An integer to skip that number of initial images.
        - limit: An integer to limit the maximum number of images displayed.
        - caption: A boolean to decide if a caption should be displayed. Captions are created from file names.

Examples
--------
These tags are equivalent:
[bg|example_gallery]
[bg|location=example_gallery]

Lightbox modules
----------------
Support for the lightbox modules Colorbox, Lightbox2, and Shadowbox is provided.  You may add support for any other lightbox module you require from your own custom module using hook_brilliant_gallery_lightbox_info().  The hook allows you to modify the attributes added to gallery links.  An example:

/**
 * Implements hook_brilliant_gallery_lightbox_info().
 */
function mymodule_brilliant_gallery_lightbox_info($setname) {
  $info['my_lightbox'] = array(
    'title' => t('My Lightbox'),
    'attributes' => array(
      'class' => 'my-lightbox',
      'rel' => $setname,
    ),
  );

  return $info;
}

Credits
-------
Brilliant Gallery was originally created by Tomas Fulopp (Vacilando).
This Drupal 7 rewrite was created by Brendan Andersen (gilgabar).