<?php

/**
 * @file
 *   Classes to create brilliant galleries.
 */

/**
 * Sanitize parameters, add any missing parameters, and remove any extraneous parameters.
 */
class BrilliantGalleryParameters {
  public $location, $recursive, $thumb, $full, $sort, $offset, $limit, $caption;

  /**
   * Constructor.
   *
   * @param $parameters
   *   Either a string formatted as a brilliant gallery tag or an associative array
   *   of parameters.
   */
  public function __construct($parameters) {
    // Handle tags.
    if (is_string($parameters)) {
      $parameters = $this->parseTag($parameters);
    }

    // Set the parameters.
    if (is_array($parameters)) {
      $this->setParameters($parameters);
    }
    else {
      throw new Exception(t('Invalid parameters passed to Brilliant Gallery.'));
    }
  }

  /**
   * Parse a brilliant gallery tag into an array.
   *
   * @param $tag
   *   A string representing a brilliant gallery tag.
   * @return
   *   An associative array of parameters.
   */
  protected function parseTag($tag) {
    $parameters = array();

    // Strip the tag down to the parameters. Remove HTML tags. Create an array of parameters.
    $tag_parameters = explode('|', strip_tags(substr($tag, 4, -1)));

    // New style bg tags of the form: [bg|parameter1=value1|parameter2=value2].
    if (strpos($tag_parameters[0], '=')) {
      foreach ($tag_parameters as $tag_parameter) {
        $value = explode('=', $tag_parameter);
        $parameters[trim($value[0])] = trim($value[1]);
      }
    }
    // Old style bg tags of the form: [bg|value1|value2].
    else {
      $ordered_parameter_keys = array('location', 'columns', 'width', 'sort', 'limit', 'color', 'offset', 'caption');
      $key_count = count($ordered_parameter_keys);
      $tag_count = count($tag_parameters);
      $size = ($key_count > $tag_count) ? $tag_count : $key_count;
      $ordered_parameter_keys = array_slice($ordered_parameter_keys, 0, $size);
      $tag_parameters = array_slice($tag_parameters, 0, $size);
      $parameters = array_combine($ordered_parameter_keys, $tag_parameters);
    }

    return $parameters;
  }

  /**
   * Get the default parameters.
   *
   * @see BrilliantGalleryParameters::setParameters().
   */
  public function getDefaultParameters() {
    return array(
      'location' => '',
      'recursive' => variable_get('brilliant_gallery_recursive', FALSE),
      'thumb' => variable_get('brilliant_gallery_thumb', 'thumbnail'),
      'full' => variable_get('brilliant_gallery_full', 'original'),
      'sort' => variable_get('brilliant_gallery_sort', TRUE),
      'offset' => 0,
      'limit' => 0,
      'caption' => variable_get('brilliant_gallery_caption', FALSE),
    );
  }

  /**
   * Get the sanitized parameters in a form appropriate to pass as a cache id.
   */
  public function getCacheId() {
    return array(
      $this->location,
      $this->recursive,
      $this->thumb,
      $this->full,
      $this->sort,
      $this->offset,
      $this->limit,
      $this->caption,
    );
  }

  /**
   * Set the parameters for the gallery.
   *
   * @param $parameters
   *   An associative array of parameters.
   *   - location: The location of the images. Usually a directory path. Also '*' or '?'.
   *   - recursive: A boolean to decide if images in subfolders should also be retrieved.
   *   - thumb: The image style to use to format the thumbnail size image.
   *   - full: The image style to use to format the full size image.
   *   - sort: A boolean to decide if the images should be sorted alphabetically by filename or shuffled randomly.
   *   - offset: An integer to skip that number of initial images.
   *   - limit: An integer to limit the maximum number of images displayed.
   *   - caption: A boolean to decide if a caption should be displayed.
   */
  protected function setParameters($parameters) {
    // Get a complete set of parameters.  Override defaults and prune any extra keys not present in defaults.
    $default_parameters = $this->getDefaultParameters();
    $overidden_parameters = array_replace($default_parameters, $parameters);
    $pruned_parameters = array_intersect_key($overidden_parameters, $default_parameters);
    extract($pruned_parameters);

    // Set and sanitize properties from the parameters.
    $this->location = check_plain($location);
    $this->recursive = $this->checkBool($recursive);
    $this->thumb = check_plain($thumb);
    $this->full = check_plain($full);
    $this->sort = $this->checkBool($sort);
    $this->offset = $this->checkInt($offset);
    $this->limit = $this->checkInt($limit);
    $this->caption = $this->checkBool($caption);
  }

  /**
   * Convert boolean string equivalents to boolean.
   * Allows strings like 'true' or 'false' to map to the correct boolean values.
   */
  protected function checkBool($value) {
    return filter_var($value, FILTER_VALIDATE_BOOLEAN);
  }

  /**
   * Sanitize integer values.
   */
  protected function checkInt($value) {
    return filter_var($value, FILTER_SANITIZE_NUMBER_INT);
  }

}


/**
 * Create a renderable array of gallery items for a set of parameters.
 */
class BrilliantGallery {
  protected $params;
  protected $path;
  protected $files = array();
  protected $directories = array();
  protected $hidden;

  /**
   * Constructor.
   */
  public function __construct(BrilliantGalleryParameters $parameters) {
    $this->params = $parameters;
    $this->setFiles();

    if ($this->isEmpty()) {
      throw new Exception(t('No files found at the specified location.'));
    }
    else {
      // Hide the hidden files.
      $this->setHidden();

      // Sort the files
      if ($this->params->sort) {
        $this->sortByUri();
      }
      else {
        $this->shuffle();
      }

      // Apply offset and limit.
      if ($this->params->offset > 0 || $this->params->limit > 0) {
        $this->slice($this->params->offset, $this->params->limit);
      }
    }
  }

  /**
   * Set the files property.
   */
  protected function setFiles() {
    $path = 'public://' . variable_get('brilliant_gallery_folder', '');

    // Handle special locations.
    if ($this->params->location == '*') {
      // The '*' character indicates the user wants to display everything in the
      // root galleries directory. Nothing needs to happen. Use the root path as is.
    }
    elseif ($this->params->location == '?') {
      // The '?' character indicates the user wants to display a random gallery.
      // @TODO: allow random galleries within a subdirectory to be selected.
      $this->load($path, TRUE, TRUE);
      $path = $this->getRandomGallery();
    }
    else {
      // Otherwise load the files from the specified location.
      $path .= '/' . $this->params->location;
    }

    $this->path = $path;
    $this->load($path, $this->params->recursive);
  }

  /**
   * Get a list of files and directories under a particular path.
   *
   * @param $path
   *   The directory from which to start searching.
   * @param $recursive
   *   Boolean if it should recursively scan directories within the directories it finds.
   * @param $list_directories
   *   Boolean if it should skip files and only load a list of directories.
   */
  protected function load($path, $recursive, $list_directories = FALSE) {
    // Make sure the specified path exists.
    if (is_dir($path) === FALSE) {
      return;
    }

    $invisible_file_names = array('.', '..');
    $stack[] = $path;

    while ($stack) {
      $directory = array_pop($stack);
      // Get a list of the files in this directory.
      if ($directory_contents = scandir($directory)) {
        foreach ($directory_contents as $content) {
          if (!in_array($content, $invisible_file_names)) {
            $uri = $directory . '/' . $content;
            if (!$list_directories && is_file($uri) && is_readable($uri) && $this->isAllowedFiletype($uri)) {
              // This path is a file. Add it to the list of files.
              $this->files[] = new BrilliantGalleryFile($uri);
            }
            elseif (is_dir($uri) && is_readable($uri)) {
              // This path is a directory. Add it to the list of directories.
              $this->directories[] = $uri;
              // Add this path to the stack if recursion is enabled.
              if ($recursive) {
                $stack[] = $uri;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Check if any files were found.
   */
  public function isEmpty() {
    return empty($this->files);
  }

  /**
   * Get the list of files.
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * Get the list of directories.
   */
  public function getDirectories() {
    return $this->directories;
  }

  /**
   * Slice the files list.
   *
   * @param $offset
   *   An offset.
   * @param $limit
   *   A limit
   */
  protected function slice($offset, $limit) {
    // A limit of zero makes no sense (it returns nothing), so set it to NULL to prevent that.
    $limit = ($limit == 0) ? NULL : $limit;
    $this->files = array_slice($this->files, $offset, $limit);
  }

  /**
   * Sort the files list randomly.
   */
  protected function shuffle() {
    shuffle($this->files);
  }

  /**
   * Sort the files list alphabetically by URI.
   */
  protected function sortByUri() {
    if ($this->files) {
      usort($this->files, 'strcasecmp');
    }
    natcasesort($this->directories);
  }

  /**
   * Get a random item from the files list.
   */
  protected function getRandomGallery() {
    shuffle($this->directories);
    return $this->directories[0];
  }

  /**
   * Set hidden files.
   */
  protected function setHidden() {
    // Query the brilliant_gallery_hidden table to find all rows that have a path in $this->directories or $this->path.
    $paths = $this->directories;
    $paths[] = $this->path;
    $result = db_query('SELECT * FROM {brilliant_gallery_hidden} WHERE path IN (:paths)', array(':paths' => $paths));
    foreach ($result as $row) {
      $uri = $row->path . '/' . $row->filename;
      $this->hidden[$uri] = $uri;
    }

    // Loop through the list of files and set the hidden flag on any that should be hidden.
    if (!empty($this->hidden)) {
      foreach ($this->files as $file) {
        if (isset($this->hidden[$file->uri])) {
          $file->setHidden();
        }
      }
    }
  }

  /**
   * Get an array of items to add to a render array.
   */
  public function getRenderItems() {
    $items = array();

    $setname = mt_rand(1, 9999999);
    $attributes = brilliant_gallery_get_lightbox_attributes($setname);

    foreach ($this->files as $file) {
      if (!$file->isHidden()) {
        $file->buildRenderElement($this->params, $attributes);
        $items[] = array(
          'data' => $file->renderElement,
        );
      }
    }

    return $items;
  }

  /**
   * Check if a file is one that brilliant gallery can use.
   *
   * @param $filename
   *   The name of the file to check.
   * @return
   *   Whether the file is of an allowed type.
   */
  protected function isAllowedFiletype($filename) {
    $extension = $this->parseExtension($filename);
    $allowed = array('jpg', 'jpeg', 'gif', 'png');
    if (in_array($extension, $allowed)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
  * Parse the file type extension from a file name.
  *
  * @param $imagename
  *   The name of the file.
  * @return
  *   The file type extension.
  */
  protected function parseExtension($imagename) {
    $imagename = explode(".", $imagename);
    return strtolower(array_pop($imagename));
  }

}


/**
 * Create a file object.
 */
class BrilliantGalleryFile {
  public $uri, $filename, $path;
  public $renderElement = array();
  protected $hidden = FALSE;

  /**
   * Constructor.
   */
  public function __construct($uri) {
    $this->uri = $uri;
    $this->setFilename();
  }

  /**
   * Make sorting easier.
   */
  public function __toString() {
    return $this->uri;
  }

  /**
   * Set the filename.
   */
  protected function setFilename() {
    $parts = explode('/', $this->uri);
    $this->filename = array_pop($parts);
    $this->path = implode('/', $parts);
  }

  /**
   * Get a caption.
   */
  protected function getCaption() {
    $parts = explode('.', $this->filename);
    array_pop($parts);
    $name = implode('.', $parts);
    $replace = array('_', '.');
    return str_replace($replace, ' ', $name);
  }

  /**
   * Save a hidden uri to the database.
   */
  public function hide() {
    db_insert('brilliant_gallery_hidden')
      ->fields(array(
        'path' => $this->path,
        'filename' => $this->filename,
      ))
      ->execute();
  }

  /**
   * Delete a hidden uri from the database.
   */
  public function show() {
    db_delete('brilliant_gallery_hidden')
      ->condition('path', $this->path)
      ->condition('filename', $this->filename)
      ->execute();
  }

  /**
   * Delete the file.
   */
  public function delete() {
    file_unmanaged_delete($this->uri);
  }

  /**
   * Set a flag to indicate that this file should be hidden.
   *
   * @see BrilliantGallery::setHidden().
   */
  public function setHidden() {
    $this->hidden = TRUE;
  }

  /**
   * Check if this file is hidden.
   */
  public function isHidden() {
    return $this->hidden;
  }

  /**
   * Check if the file exists.
   */
  public function isFile() {
    return is_file($this->uri);
  }

  /**
   * Build a render element for this file. The element will render
   * a thumbnail image linked to a full size image.
   *
   * @param $params
   *   A BrilliantGalleryParameters object.
   * @param $attributes
   *   The lightbox specific attributes.
   */
  public function buildRenderElement($params, $attributes = array()) {
    $full_uri = $params->full == 'original' ? file_create_url($this->uri) : image_style_url($params->full, $this->uri);

    // Build the render array for this link.
    $this->renderElement = array(
      '#type' => 'link',
      '#title' => array(
        '#theme' => 'image_style',
        '#style_name' => $params->thumb,
        '#path' => $this->uri,
      ),
      '#href' => $full_uri,
      '#options' => array(
        'html' => TRUE,
        'attributes' => $attributes,
      ),
      '#pre_render' => array_merge(array('brilliant_gallery_link_image_child_render'), element_info_property('link', '#pre_render', array())),
    );

    // Add a caption.
    if ($params->caption) {
      $this->renderElement['#options']['attributes'] += array('title' => $this->getCaption());
    }
  }

}
