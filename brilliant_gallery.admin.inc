<?php

/**
* @file
*   Administrative functionality for brilliant gallery.
*/

/**
 * Implements Form API for the admin settings form.
 */
function brilliant_gallery_admin_settings($form, &$form_state) {
  // Load lightbox options.
  $lightbox_options = array();
  foreach (brilliant_gallery_lightbox_info_load() as $key => $value) {
    $lightbox_options[$key] = $value['title'];
  }

  // Load image style options.
  $style_options = array();
  foreach (image_styles() as $key => $value) {
    $style_options[$key] = $value['name'];
  }
  $style_options += array('original' => 'original');

  $form['brilliant_gallery_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['brilliant_gallery_general']['brilliant_gallery_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the main gallery folder'),
    '#default_value' => variable_get('brilliant_gallery_folder', ''),
    '#size' => 77,
    '#required' => TRUE,
    '#maxlength' => 333,
    '#description' => t("Path to the main folder in which your individual gallery folders will be placed. The folder must exist under your files folder. Exclude trailing slashes. Example: <i>albums</i>."),
  );

  $form['brilliant_gallery_general']['brilliant_gallery_admin_thumb'] = array(
    '#type' => 'select',
    '#title' => t('Admin manage thumbnail style'),
    '#description' => t('Choose the image style to use for thumbnails on admin management pages. Visit !link to add or modify styles.', array('!link' => l('the styles settings page', 'admin/config/media/image-styles'))),
    '#options' => $style_options,
    '#default_value' => variable_get('brilliant_gallery_admin_thumb', 'thumbnail'),
  );

  $form['brilliant_gallery_general']['brilliant_gallery_lightbox'] = array(
    '#type' => 'select',
    '#title' => t('Lightbox plugin'),
    '#required' => FALSE,
    '#options' => $lightbox_options,
    '#default_value' => variable_get('brilliant_gallery_lightbox', 'colorbox'),
    '#description' => t('The selected plugin must be installed in order to have an effect.'),
  );

  $form['brilliant_gallery_defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default gallery settings'),
    '#description' => t('These settings will be used if not overridden by individual galleries.'),
  );

  $form['brilliant_gallery_defaults']['brilliant_gallery_thumb'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail style'),
    '#description' => t('Choose the image style to use for thumbnail image display. Visit !link to add or modify styles.', array('!link' => l('the styles settings page', 'admin/config/media/image-styles'))),
    '#options' => $style_options,
    '#default_value' => variable_get('brilliant_gallery_thumb', 'thumbnail'),
  );

  $form['brilliant_gallery_defaults']['brilliant_gallery_full'] = array(
    '#type' => 'select',
    '#title' => t('Full size style'),
    '#description' => t('Choose the image style to use for full size image display. Visit !link to add or modify styles.', array('!link' => l('the styles settings page', 'admin/config/media/image-styles'))),
    '#options' => $style_options,
    '#default_value' => variable_get('brilliant_gallery_full', 'original'),
  );

  $form['brilliant_gallery_defaults']['brilliant_gallery_sort'] = array(
    '#type' => 'radios',
    '#title' => t('Image order'),
    '#default_value' => variable_get('brilliant_gallery_sort', '1'),
    '#options' => array(
      '1' => t('Sort images alphabetically by their file names.'),
      '0' => t('Randomize image order (on each page load or cache refresh).'),
    ),
  );

  $form['brilliant_gallery_defaults']['brilliant_gallery_recursive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Retrieve images recursively.'),
    '#default_value' => variable_get('brilliant_gallery_recursive', '0'),
    '#description' => t("Check this if you want to include images from child directories."),
  );

  $form['brilliant_gallery_defaults']['brilliant_gallery_caption'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display file name as caption'),
    '#default_value' => variable_get('brilliant_gallery_caption', '0'),
    '#description' => t("Check this if you want the lightbox to display a caption based on the image file name (dots and underscores are automatically replaced by spaces)."),
  );

  return system_settings_form($form);
}

/**
 * Page callback for admin gallery management.
 */
function brilliant_gallery_admin_manage() {
  $directories = array();

  try {
    $parameters = array(
      'location' => '*',
      'recursive' => TRUE,
    );
    $params = new BrilliantGalleryParameters($parameters);
    $gallery = new BrilliantGallery($params);
    $directories = $gallery->getDirectories();
  }
  catch (Exception $e) {}

  $header = array(
    t('Galleries'),
    t('Operations'),
  );

  $rows = array();
  foreach ($directories as $directory) {
    $directory = _brilliant_gallery_prune_path($directory);
    $url_directory = urlencode($directory);
    $rows[] = array(
      array('data' => array(
        '#type' => 'link',
        '#title' => $directory,
        '#href' => "admin/content/brilliant_gallery/$url_directory",
      )),
      array('data' => array(
        'edit' => array(
          '#type' => 'link',
          '#title' => 'edit',
          '#href' => "admin/content/brilliant_gallery/$url_directory",
          '#prefix' => '<div>',
          '#suffix' => '</div>',
        ),
        'delete' => array(
          '#type' => 'link',
          '#title' => 'delete',
          '#href' => "admin/content/brilliant_gallery/$url_directory/delete",
          '#prefix' => '<div>',
          '#suffix' => '</div>',
        ),
      )),
    );
  }

  $table = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array(),
    '#caption' => NULL,
    '#colgroup' => array(),
    '#sticky' => TRUE,
    '#empty' => t('There are no galleries to display.'),
  );

  return $table;
}

/**
 * Page callback for admin gallery editing.
 */
function brilliant_gallery_admin_manage_edit($directory) {
  $files = array();
  $directory = urldecode($directory);
  $attributes = brilliant_gallery_get_lightbox_attributes();

  try {
    $parameters = array(
      'location' => $directory,
      'recursive' => TRUE,
      'thumb' => variable_get('brilliant_gallery_admin_thumb', 'thumbnail'),
      'full' => 'original',
    );
    $params = new BrilliantGalleryParameters($parameters);
    $gallery = new BrilliantGallery($params);
    $files = $gallery->getFiles();
  }
  catch (Exception $e) {}

  $header = array(
    t('Thumbnail'),
    t('Filename'),
    t('Visibility'),
    t('Operations'),
  );

  $rows = array();
  foreach ($files as $file) {
    $action = ($file->isHidden() ? 'show' : 'hide');
    $file->buildRenderElement($params, $attributes);
    $url_file = urlencode(_brilliant_gallery_prune_path($file->uri));
    $rows[] = array(
      array('data' => $file->renderElement),
      array('data' => $file->filename),
      array(
        'data' => ($file->isHidden() ? 'Hidden' : 'Visible'),
        'class' => 'bg-visibility-status',
      ),
      array('data' => array(
        'visibility' => array(
          '#type' => 'link',
          '#title' => ($file->isHidden() ? 'show' : 'hide'),
          '#href' => 'admin/content/brilliant_gallery/visibility',
          '#options' => array(
            'query' => array(
              'action' => $action,
              'uri' => $url_file,
              'token' => drupal_get_token('brilliant-gallery-visibility'),
            ),
          ),
          '#prefix' => '<div>',
          '#suffix' => '</div>',
          '#attributes' => array(
            'class' => array('bg-visibility-ajax'),
          ),
        ),
        'delete' => array(
          '#type' => 'link',
          '#title' => 'delete',
          '#href' => "admin/content/brilliant_gallery/$url_file/delete",
          '#prefix' => '<div>',
          '#suffix' => '</div>',
        ),
      )),
    );
  }

  $table = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array(
      'id' => 'bg-admin-edit',
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'brilliant_gallery') . '/brilliant_gallery.admin.js'),
    ),
    '#caption' => NULL,
    '#colgroup' => array(),
    '#sticky' => TRUE,
    '#empty' => t('There are no images to display.'),
  );

  drupal_set_title('Manage gallery: '. $directory);

  return $table;
}

/**
 * AJAX callback for managing visibility.
 */
function brilliant_gallery_admin_manage_visibility() {
  if (drupal_valid_token($_GET['token'], 'brilliant-gallery-visibility')) {
    $action = check_plain($_GET['action']);
    $uri = 'public://' . variable_get('brilliant_gallery_folder', '') . '/' . filter_xss(urldecode($_GET['uri']));
    $file = new BrilliantGalleryFile($uri);

    if (in_array($action, array('show', 'hide')) && $file->isFile()) {
      switch ($action) {
        case 'show':
          $file->show();
          break;
        case 'hide':
          $file->hide();
          break;
      }

      return drupal_json_output(TRUE);
    }
  }

  // The request failed.
  watchdog('Brilliant Gallery', 'Brilliant Gallery received an invalid visibility update request. Action: %action, URI: %uri', array('%action' => $action, '%uri' => $uri));
  drupal_json_output(FALSE);
}

/**
 * Page callback to confirm delete request.
 */
function brilliant_gallery_delete_confirm($form, &$form_state, $uri) {
  $uri = filter_xss(urldecode($uri));
  $path = 'public://' . variable_get('brilliant_gallery_folder', '') . '/' . $uri;

  $form['uri'] = array(
    '#type' => 'value',
    '#value' => $uri,
  );

  $form['path'] = array(
    '#type' => 'value',
    '#value' => $path,
  );

  if (is_dir($path)) {
    return confirm_form($form,
      t('Are you sure you want to delete the gallery %title?', array('%title' => $uri)),
       'admin/content/brilliant_gallery',
      t('This will also delete any images or other files in the gallery.  This action cannot be undone.'),
      t('Delete'),
      t('Cancel')
    );
  }
  elseif (is_file($path)) {
    return confirm_form($form,
      t('Are you sure you want to delete the image %title?', array('%title' => $uri)),
       'admin/content/brilliant_gallery',
      t('This action cannot be undone.'),
      t('Delete'),
      t('Cancel')
    );
  }
  else {
    $form['bg_delete_error'] = array(
      '#markup' => t('The path %uri is not a valid gallery or image.', array('%uri' => $uri)),
    );

    $form['bg_delete_return'] = array(
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => l('Return to galleries', 'admin/content/brilliant_gallery'),
    );

    return $form;
  }
}

/**
 * Delete a gallery or image.
 */
function brilliant_gallery_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $uri = $form_state['values']['uri'];
    $path = $form_state['values']['path'];

    // Suppress errors from the file delete function so that we may display a more friendly message.
    if (@file_unmanaged_delete_recursive($path)) {
      watchdog('Brilliant Gallery', 'Deleted gallery item %uri.', array('%uri' => $uri));
      drupal_set_message(t('%uri has been deleted.', array('%uri' => $uri)));
    }
    else {
      watchdog('Brilliant Gallery', 'Could not delete gallery item %uri. Permission denied.', array('%uri' => $uri), WATCHDOG_ERROR);
      drupal_set_message(t('Could not delete %uri. Permission denied.', array('%uri' => $uri)), 'error');
    }
  }

  $form_state['redirect'] = 'admin/content/brilliant_gallery';
}

/**
 * Prune the stream wrapper string and brilliant gallery directory from the beginning of a path.
 */
function _brilliant_gallery_prune_path($path) {
  $prune = 'public://' . variable_get('brilliant_gallery_folder', '');
  return str_replace($prune . '/', '', $path);
}