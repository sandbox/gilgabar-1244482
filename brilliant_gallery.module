<?php

/**
 * @file
 *   Create lightbox enabled image galleries from directories of unmanaged files.
 */

/**
 * Implements hook_menu().
 */
function brilliant_gallery_menu() {
  $items = array();

  $items['admin/config/media/brilliant_gallery'] = array(
    'title' => 'Brilliant gallery',
    'description' => 'Brilliant gallery module settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('brilliant_gallery_admin_settings'),
    'access arguments' => array('access administration pages'),
    'file' => 'brilliant_gallery.admin.inc',
  );

  $items['admin/content/brilliant_gallery'] = array(
    'title' => 'Brilliant galleries',
    'description' => 'Manage galleries displayed using the Brilliant gallery module.',
    'page callback' => 'brilliant_gallery_admin_manage',
    'access arguments' => array('manage brilliant galleries'),
    'file' => 'brilliant_gallery.admin.inc',
  );

  $items['admin/content/brilliant_gallery/%'] = array(
    'title' => 'Brilliant gallery',
    'description' => 'Edit an individual gallery.',
    'page callback' => 'brilliant_gallery_admin_manage_edit',
    'page arguments' => array(3),
    'access arguments' => array('manage brilliant galleries'),
    'file' => 'brilliant_gallery.admin.inc',
  );

  $items['admin/content/brilliant_gallery/visibility'] = array(
    'title' => 'Manage image visibility',
    'page callback' => 'brilliant_gallery_admin_manage_visibility',
    'access arguments' => array('manage brilliant galleries'),
    'file' => 'brilliant_gallery.admin.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/content/brilliant_gallery/%/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete an individual gallery or image.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('brilliant_gallery_delete_confirm', 3),
    'access arguments' => array('manage brilliant galleries'),
    'file' => 'brilliant_gallery.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_help().
 */
function brilliant_gallery_help($path, $arg) {
  switch ($path) {
    case "admin/help#brilliant_gallery":
      $output = '<p>' . t("Highly customizable Drupal module producing multiple table galleries of quality-scaled images from either a pre-defined local folder, or from any public Picasa gallery.") . '</p>';
      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function brilliant_gallery_permission() {
  return array(
    'access brilliant_gallery' => array(
      'title' => t('Access Brilliant Gallery'),
      'description' => t('Allow a user to view galleries.'),
    ),
    'manage brilliant galleries' => array(
      'title' => t('Manage Brilliant Galleries'), 
      'description' => t('Manage Brilliant Galleries.'),
    ),
  );
}

/**
 * Implements hook_filter_info().
 */
function brilliant_gallery_filter_info() {
  $filters = array();

  $filters['brilliant_gallery'] = array(
    'title' => t('Brilliant Gallery Tag'),
    'description' => t('Substitutes a special Brilliant Gallery Tag with the actual gallery table.'),
    'process callback' => 'brilliant_gallery_filter_brilliant_gallery_process',
    'tips callback' => 'brilliant_gallery_filter_brilliant_gallery_tips',
  );

  return $filters;
}

/**
 * Implements hook_filter_FILTER_process().
 */
function brilliant_gallery_filter_brilliant_gallery_process($text, $filter, $format, $langcode, $cache, $cache_id) {

  // Find all the brilliant gallery tags in the text string.
  $galleries = array();
  preg_match_all("/(\[)bg(\|)[^\]]*(\])/s", $text, $matches);
  $bg_tags = $matches[0];

  // Return the text unaltered if there are no tags.
  if (empty($bg_tags)) {
    return $text;
  }

  // Render a gallery for each set of tags.
  foreach ($bg_tags as $bg_tag) {
    $galleries[] = brilliant_gallery_render($bg_tag);
  }

  return str_replace($bg_tags, $galleries, $text);
}

/**
 * Implements hook_flush_caches().
 */
function brilliant_gallery_flush_caches() {
  return array('cache_brilliant_gallery');
}

/**
 * Render a gallery.
 *
 * @param $parameters
 *   A string formatted as a brilliant gallery tag or an associative array of parameters.
 *   @see BrilliantGalleryParameters::setParameters().
 * @return
 *   A rendered gallery: an unordered list of thumbnail images with links to full size images.
 */
function brilliant_gallery_render($parameters) {
  $params = new BrilliantGalleryParameters($parameters);

  $render_array = array(
    'params' => $params,
    '#access' => user_access('access brilliant_gallery'),
    '#theme' => 'item_list__brilliant_gallery',
    '#items' => array(),
    '#attributes' => array(
      'class' => 'brilliant-gallery',
    ),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'brilliant_gallery') . '/brilliant_gallery.css'),
    ),
    '#pre_render' => array(
      'brilliant_gallery_render_items',
      'brilliant_gallery_item_list_child_render',
    ),
    '#cache' => array(
      'keys' => $params->getCacheId(),
      'bin' => 'cache_brilliant_gallery',
    ),
  );

  return drupal_render($render_array);
}

/**
 * Pre_render function to set the #items property of a gallery render array.
 */
function brilliant_gallery_render_items($element) {
  // Try to load the gallery.
  try {
    $gallery = new BrilliantGallery($element['params']);
    $element['#items'] = $gallery->getRenderItems();
  }
  catch (Exception $e) {
    watchdog('Brilliant Gallery', 'No displayable images were found for the path, %path.  This is probably due to a typo in the Brilliant Gallery tag used to load that path or the files or directory at that path may have been moved.', array('%path' => $element['params']->location));
  }

  // Remove the parameters object.
  unset($element['params']);

  return $element;
}

/**
 * Implements hook_filter_FILTER_tips().
 */
function brilliant_gallery_filter_brilliant_gallery_tips($filter, $format, $long) {
  if ($long) {
    return t('Substitutes a special Brilliant Gallery Tag with the actual gallery table.');
  }
  else {
    return t('Substitutes a special Brilliant Gallery Tag with the actual gallery table.');
  }
}

/**
 * Load an array of all the lightbox settings.
 */
function brilliant_gallery_lightbox_info_load($setname = '') {
  $info = &drupal_static(__FUNCTION__);

  if (empty($info)) {
    $info = module_invoke_all('brilliant_gallery_lightbox_info', $setname);
    drupal_alter('brilliant_gallery_lightbox_info', $info, $setname);
  }

  return $info;
}

/**
 * Implements hook_brilliant_gallery_lightbox_info().
 */
function brilliant_gallery_brilliant_gallery_lightbox_info($setname) {
  $info['colorbox'] = array(
    'title' => t('Colorbox'),
    'attributes' => array(
      'class' => 'colorbox',
      'rel' => $setname,
    ),
  );

  $info['lightbox'] = array(
    'title' => t('Lightbox'),
    'attributes' => array(
      'rel' => 'lightbox[' . $setname . ']',
    ),
  );

  $info['shadowbox'] = array(
    'title' => t('Shadowbox'),
    'attributes' => array(
      'rel' => 'shadowbox[' . $setname . ']',
    ),
  );

  $info['none'] = array(
    'title' => t('None'),
    'attributes' => array(),
  );

  return $info;
}

/**
 * Get lightbox attributes.
 *
 * @param $setname
 *   The name used to group sets of photos into a gallery.
 */
function brilliant_gallery_get_lightbox_attributes($setname = 'gallery') {
  $lightbox_info = brilliant_gallery_lightbox_info_load($setname);
  $lightbox = variable_get('brilliant_gallery_lightbox', 'colorbox');
  $lightbox_attributes = array();
  if (isset($lightbox_info[$lightbox]['attributes'])) {
    $lightbox_attributes = $lightbox_info[$lightbox]['attributes'];
  }

  return $lightbox_attributes;
}

/**
 * Allow renderable children in an item list. Workaround for http://drupal.org/node/891112.
 */
function brilliant_gallery_item_list_child_render($elements) {
  foreach (array_keys($elements['#items']) as $key) {
    if (is_array($elements['#items'][$key]['data'])) {
      $elements['#items'][$key]['data'] = drupal_render($elements['#items'][$key]['data']);
    }
  }

  return $elements;
}

/**
 * Allow renderable child image as link title.
 */
function brilliant_gallery_link_image_child_render($elements) {
  if (is_array($elements['#title'])) {
    $elements['#title'] = drupal_render($elements['#title']);
  }

  return $elements;
}
